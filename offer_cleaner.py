import re
import gumtree_parser as gt_parser
from bs4 import BeautifulSoup  # For HTML parsing

class posting:
    def __init__(self, address, price, link, title, content):
        self.address = address
        self.price = price
        self.link = link
        self.title= title
        self.content = content

    def __str__(self):
        return "%s,%s,%s,%s,%s" % (self.price, self.address, self.title, self.link, self.content)
    
def html_to_posting(soup):
    pure = soup.find('div', class_='description').get_text().strip()
    content = re.sub(r'[\W ]+', ' ', pure)
    pure = re.sub(r'\.([^ ])', r'. \1', pure)
    title = soup.title.string.replace(",", " ").strip()
    price = soup.find('span', class_='amount').get_text().replace("\xa0", "")
    link = soup.find('meta', property='og:url')['content']
    address = re.search('((os|ul|al|ulica|ulicy|aleja|alei|aleji|rondo|rondzie|ronda))\.? \w*.?.?\w*',pure, re.IGNORECASE)
    if address is not None:
        address = address.group(0)
#        print(address)
    else:
        address = re.search('( (osiedlu|osiedle|osiedla|oś|os|ul|al|ulica|ulicy|aleja|alei|aleji|rondo|rondzie|ronda))\.? \w*.?.?\w*',title, re.IGNORECASE)
        if address is not None:
            address = address.group(0)
#            print(address)
        else:
            address = ''
#            print(pure)
#            print(title)
#            print(link)
    return posting(address, price, link, title, content)

def save_csv(list_of_postings, filename):
    file_path = "./" + filename
    gt_parser.ensure_dir(file_path)
    with open(file_path, "w", encoding='utf-8') as file:
        file.write("cena, adres, tytuł, link, tresc")
        for posting in postings_processed:
            file.write("\n"+str(posting))
    

print("Began processing")
postings = list(set(gt_parser.get_postings()))
postings_processed = list(map(html_to_posting, postings))
save_csv(postings_processed, "gumtree_oferty.csv")
