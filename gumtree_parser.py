# This Python file uses the following encoding: utf-8
import os
import re  # Regular expressions
import urllib.error
import urllib.request  # Website connections
from bs4 import BeautifulSoup  # For HTML parsing


def get_postings():
    searchpage_link = construct_searchpage_link()
    print("Searchpage link: " + searchpage_link)
    soup = open_page(searchpage_link)
    # Now find out how many jobs there were
    postings_count_description = soup.p.span.get_text()[8:-4]
    postings_count = re.findall(r'\d+', postings_count_description)[-1]
    num_pages = int(postings_count)//20
    print('There were', postings_count, 'postings found.')  # Display how many jobs were found
    # we need to iterate over each new #search result page
    posting_descriptions = []  # Store all our descriptions in this list
    for i in range(1, num_pages):  # Loop through all of our search result pages
        current_page = construct_ith_page_link(i)
        print('Getting page ' + str(i) + " from link " + current_page)
        page_obj = open_page(current_page)
        posting_urls = get_postings_urls(page_obj)
        posting_descriptions.append(get_postings_descriptions(posting_urls))
    print('Done with collecting the job postings!')
    print('There were', len(posting_descriptions), 'jobs successfully found.')
    return posting_descriptions  # End of the function

def construct_searchpage_link():
    searchpage_link = construct_ith_page_link(1)
    return searchpage_link

def construct_ith_page_link(i):
    final_site_prefix = 'https://www.gumtree.pl/s-mieszkania-i-domy-do-wynajecia/krakow'
    final_site_middle = '/v1c9008l3200208p1'
    final_site_suffix = ''
    result = ''.join([final_site_prefix,
                      final_site_middle[:-1],
                      str(i),
                      final_site_suffix])
    return result

def get_postings_urls(page_obj):
    postings_hyper_list = page_obj.find_all('div', class_="view")
    posting_urls = []
    for postings_list in postings_hyper_list:
        postings_list = postings_list.find_all('a')
        for posting in postings_list:
            r = re.search('href=".*"', str(posting))
            posting_urls = posting_urls + ['https://www.gumtree.pl'+str(posting)[r.start()+6:r.end()-1]]
    
    posting_urls = set([link for link in posting_urls if "mieszkania" in link])
    return posting_urls

def get_postings_descriptions(posting_urls):
    posting_descriptions = list()
    for index, posting_url in enumerate(posting_urls):
        print("Getting posting " + str(index) + " from link:" + posting_url)
        posting = open_page(posting_url)
        is_valid_posting = ( posting is not None
                               and posting.title is not None
                               and posting.title.string is not None
                           )
        if is_valid_posting:
            posting_descriptions.append(posting)
    return posting_descriptions
    
def open_page(link):
    try:
        # Open up the front page of our search first
        html = urllib.request.urlopen(link).read()  
    except:
        'Failed to open link ' + link
        return
    soup = BeautifulSoup(html, "html.parser")  # Get the html from the first page
    return soup

def save_raw_posting(final_description_soup):
    file_name = final_description_soup.title.string.replace("/", "|")
    if len(file_name) > 250:
        file_name = file_name[:250]
    file_path = make_dir_path('mieszkania', 'Kraków') + re.sub(r'([^\s\w]|_  )+', '', file_name).strip()
    ensure_dir(file_path)
    # Rozpoznawanie tytułów nie zawsze dobrze działa, a chcemy pobrać wszystkie oferty.
    counter = 0
    while os.path.isfile(file_path + ".html"):
        counter = counter + 1
        file_path = file_path + str(counter)
    file_path = file_path + ".html"
    with open(file_path, "w", encoding='utf-8') as file:
        file.write(str(final_description_soup))

def ensure_dir(file_path):
    directory = os.path.dirname(file_path)
    if not os.path.exists(directory):
        os.makedirs(directory)


def make_dir_path(job, city):
    return "./" + city + "," + job + "/"
